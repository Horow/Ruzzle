# Ruzzle

## Présentation

Ceci est un **projet scolaire** réalisé en 3 mois en Licence 3 Informatique par un groupe de **3 personnes**. Les consignes étaient de coder le jeu [Ruzzle](http://ruzzle-game.com/) en **C**, en utilisant la librairie **SDL 2.0**.

L'objectif du joueur est de faire le plus haut **score** dans le **temps imparti** (2 minutes), en reliant les **lettres** d'une **grille** afin de former des **mots**.

Dans le dossier **doc**, vous pourrez trouver un **diagramme** récapitulant les diverses entités du jeu, ainsi que le **rapport du projet** qui contient des explications plus précises sur les choix techniques, l'organisation, etc...

## Captures d'écran

Quelques interfaces du jeu :

![Interfaces](img/Interfaces.PNG)

Tous les assets ont été réalisés par nos soins (excepté le logo de Ruzzle).

![Assets de la lettre A](img/Assets.PNG)